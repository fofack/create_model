<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisiteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visiteurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('prenom');
            $table->string('pays');
            $table->string('ville');
            $table->string('tel');
            $table->unsignedInteger('reservat_id');
            $table->foreign('reservat_id')->references('num_rev')->on('reservations')->onDelete('cascade');
            $table->unsignedInteger('command_id');
            $table->foreign('command_id')->references('num_cmd')->on('commands')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visiteurs');
    }
}
